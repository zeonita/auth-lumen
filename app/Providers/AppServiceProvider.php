<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // //https://laravel.com/docs/5.6/upgrade
        // Passport::withoutCookieSerialization();
    }
}
