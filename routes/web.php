<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
    'middleware' => 'client'//'auth:api'
], function ($router) {
    $router->get('/test-password', function () {
        return 'it worked';
    });

    $router->get('/test', function () use ($router) {
        return "hello world";
    });
});
